package com.example.demo.sub;

import org.springframework.data.repository.CrudRepository;

public interface AvailablePointRedisRepository extends CrudRepository<AvailablePoint, String> {
}
